package com.frx01.yygh;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author frx
 * @version 1.0
 * @date 2022/11/12  17:49
 */
@SpringBootApplication
public class ModelApplication {
    public static void main(String[] args) {
        SpringApplication.run(ModelApplication.class,args);
    }
}
