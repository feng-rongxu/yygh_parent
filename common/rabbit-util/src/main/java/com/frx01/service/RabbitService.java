package com.frx01.service;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author frx
 * @version 1.0
 * @date 2022/11/12  14:20
 */
@Service
public class RabbitService {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * 发送消息
     * @param exchange    交换机
     * @param rootingKey  路由键
     * @param message     消息
     * @return
     */
    public boolean sendMessage(String exchange,String rootingKey,Object message){
        rabbitTemplate.convertAndSend(exchange,rootingKey,message);
        return true;
    }
}
