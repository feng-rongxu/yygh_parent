package com.frx01.yygh.utils;

import com.frx01.yygh.helper.JwtHelper;

import javax.servlet.http.HttpServletRequest;

/**
 * @author frx
 * @version 1.0
 * @date 2022/11/8  12:07
 * desc:获取当前用户信息的工具类
 */
public class AuthContextHolder {

    //获取当前用户的id
    public static Long getUserId(HttpServletRequest request){
        //从header获取到token
        String token = request.getHeader("token");
        //使用jwt从token获取userid
        Long userId = JwtHelper.getUserId(token);
        return userId;
    }

    //获取当前用户的名称
    public static String getUserName(HttpServletRequest request){
        //从header获取到token
        String token = request.getHeader("token");
        //使用jwt从token获取userName
        String userName = JwtHelper.getUserName(token);
        return userName;
    }
}
