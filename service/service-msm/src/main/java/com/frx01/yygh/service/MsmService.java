package com.frx01.yygh.service;

import com.frx01.yygh.vo.msm.MsmVo;

/**
 * @author frx
 * @version 1.0
 * @date 2022/11/3  9:40
 */
public interface MsmService {

    //发送手机验证码
    boolean send(String phone, String code);

    //mq使用发送短信
    boolean send(MsmVo msmVo);
}
