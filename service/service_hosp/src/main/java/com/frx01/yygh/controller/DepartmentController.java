package com.frx01.yygh.controller;

import com.frx01.yygh.Result;
import com.frx01.yygh.service.DepartmentService;
import com.frx01.yygh.vo.hosp.DepartmentVo;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author frx
 * @version 1.0
 * @date 2022/10/30  15:03
 */

@RestController
@RequestMapping("/admin/hosp/department")
//@CrossOrigin
public class DepartmentController {

    @Autowired
    private DepartmentService departmentService;

    //根据医院的编号，查询医院科室的列表
    @ApiOperation(value = "查询医院科室的列表")
    @GetMapping("/getDeptList/{hoscode}")
    public Result getDeptList(@PathVariable String hoscode){

       List<DepartmentVo> list = departmentService.findDeptTree(hoscode);
       return Result.ok(list);
    }
}
