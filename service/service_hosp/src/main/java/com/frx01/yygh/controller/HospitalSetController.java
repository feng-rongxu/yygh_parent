package com.frx01.yygh.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.frx01.utils.MD5;
import com.frx01.yygh.Result;
import com.frx01.yygh.handler.YyghException;
import com.frx01.yygh.model.hosp.HospitalSet;
import com.frx01.yygh.service.HospitalSetService;
import com.frx01.yygh.vo.hosp.HospitalQueryVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Random;

/**
 * @author frx
 * @version 1.0
 * @date 2022/10/20  23:35
 */
@Api("医院设置管理")
@RestController
//@CrossOrigin
@RequestMapping("/admin/hosp/hospitalSet")
public class HospitalSetController {

    @Autowired
    private HospitalSetService hospitalSetService;

    //1.查询医院设置表里面的所有信息
    @ApiOperation(value = "获取所有医院设置的信息")
    @GetMapping("/findAll")
    public Result findAllHospitalSet(){
        List<HospitalSet> list = hospitalSetService.list();
        return Result.ok(list);
    }

    //2.逻辑删除医院设置
    @ApiOperation(value = "逻辑删除医院设置")
    @DeleteMapping("{id}")
    public Result removeHospitalSet(@PathVariable Long id){
        boolean flag = hospitalSetService.removeById(id);
        if(flag){
            return Result.ok(flag);
        }
        return Result.fail();
    }

    //3.条件分页查询
    @PostMapping("/findPageHospSet/{current}/{size}")
    public Result findPageHospSet(@PathVariable long current,
                           @PathVariable long size,
                           @RequestBody(required = false) HospitalQueryVo hospitalQueryVo){
        Page<HospitalSet> page = new Page<>(current, size);

        QueryWrapper<HospitalSet> queryWrapper = new QueryWrapper<>();
        String hosname = hospitalQueryVo.getHosname();
        String hoscode = hospitalQueryVo.getHoscode();
        if(!StringUtils.isEmpty(hosname))
            queryWrapper.like("hosname",hosname);
        if(!StringUtils.isEmpty(hoscode))
            queryWrapper.eq("hoscode",hoscode);
        Page<HospitalSet> hospitalSetPage = hospitalSetService.page(page, queryWrapper);
        return Result.ok(hospitalSetPage);
    }

    //4.保存医院设置
    @PostMapping("saveHospitalSet")
    public Result saveHospitalSet(@RequestBody HospitalSet hospitalSet){

        //设置状态 1可用 0不可用
        hospitalSet.setStatus(1);
        //签名密钥
        Random random = new Random();
        hospitalSet.setSignKey(MD5.encrypt(System.currentTimeMillis()+""+random.nextInt(1000)));

        //调用service
        boolean save = hospitalSetService.save(hospitalSet);
        if(save)
            return Result.ok();
        return Result.fail();

    }

    //5.根据id获取医院设置
    @GetMapping("/getHospSet/{id}")
    public Result getHospSet(@PathVariable long id){

        try {
            int i = 10/0;
        } catch (Exception e) {
            throw new YyghException("失败",201);
        }

        HospitalSet hospitalSet = hospitalSetService.getById(id);
        return Result.ok(hospitalSet);
    }

    //6.修改医院信息
    @PostMapping("/updateHospitalSet")
    public Result updateHospitalSet(@RequestBody HospitalSet hospitalSet){
        hospitalSetService.updateById(hospitalSet);
        return Result.ok();

    }

    //7.批量删除医院设置
    @DeleteMapping("/batchRemove")
    public Result batchRemove(@RequestBody List<String> idList){
        hospitalSetService.removeByIds(idList);
        return Result.ok();
    }

    //8.医院设置锁定和解锁
    @PutMapping("/lockHospitalSet/{id}/{status}")
    public Result lockHospitalSet(@PathVariable Long id,
                                  @PathVariable Integer status){
        //先根据Id查询医院设置信息
        HospitalSet hospitalSet = hospitalSetService.getById(id);
        //设置状态
        hospitalSet.setStatus(status);
        //调用方法
        hospitalSetService.updateById(hospitalSet);
        return Result.ok();
    }

    //9.发送签名密钥
    @PutMapping("/sendKey/{id}")
    public Result lockHospitalSet(@PathVariable Long id){
        HospitalSet hospitalSet = hospitalSetService.getById(id);
        String signKey = hospitalSet.getSignKey();
        String hoscode = hospitalSet.getHoscode();
        //TODO 发送短信
        return Result.ok();
    }

}
