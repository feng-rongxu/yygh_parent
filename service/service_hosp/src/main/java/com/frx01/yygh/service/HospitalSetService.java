package com.frx01.yygh.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.frx01.yygh.model.hosp.HospitalSet;
import com.frx01.yygh.vo.order.SignInfoVo;

/**
 * @author frx
 * @version 1.0
 * @date 2022/10/20  23:31
 */
public interface HospitalSetService extends IService<HospitalSet> {

    String getSignKEY(String hoscode);

    //获取医院签名信息
    SignInfoVo getSignInfoVo(String hoscode);
}
