package com.frx01.yygh.repository;

import com.frx01.yygh.model.hosp.Department;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * @author frx
 * @version 1.0
 * @date 2022/10/27  8:43
 */
@Repository
public interface DepartmentRepository extends MongoRepository<Department,String> {

    //查询科室信息
    Department getDepartemntByHoscodeAndDepcode(String hoscode, String depcode);
}
