package com.frx01.yygh.service;

import com.frx01.yygh.model.hosp.Hospital;
import com.frx01.yygh.vo.hosp.HospitalQueryVo;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Map;

/**
 * @author frx
 * @version 1.0
 * @date 2022/10/26  16:10
 */
public interface HospitalService {
    //上传医院的方法
    void save(Map<String, Object> paramMap);

    //根据医院编号做具体查询
    Hospital getByHosCode(String hoscode);

    //条件查询分页
    Page<Hospital> selectHospPage(Integer page, Integer limit, HospitalQueryVo hospitalQueryVo);

    //更新医院的上线状态
    void updateStatus(String id, Integer status);

    //医院的详情信息
    Map<String,Object> getHospById(String id);

    //根据医院编号，获取医院名称
    String getHospName(String hoscode);

    //根据医院名称做查询
    List<Hospital> findByName(String hosname);

    // 根据医院编号获取医院预约挂号信息
    Map<String, Object> item(String hoscode);
}
