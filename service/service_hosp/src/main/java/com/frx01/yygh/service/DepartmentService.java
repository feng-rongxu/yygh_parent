package com.frx01.yygh.service;


import com.frx01.yygh.model.hosp.Department;
import com.frx01.yygh.vo.hosp.DepartmentQueryVo;
import com.frx01.yygh.vo.hosp.DepartmentVo;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Map;

/**
 * @author frx
 * @version 1.0
 * @date 2022/10/27  8:45
 */
public interface DepartmentService {
    void save(Map<String, Object> paramMap);

    Page<Department> findPageDepartment(int page, int limit, DepartmentQueryVo departmentQueryVo);

    void remove(String hoscode, String depcode);

    //根据医院的编号，查询医院科室的列表
    List<DepartmentVo> findDeptTree(String hoscode);

    //根据科室的编号 医院的编号 查询科室的名称
    String getDepName(String hoscode, String depcode);

    //根绝科室编号，和医院编号，查询科室
    Department getDepartment(String hoscode, String depcode);
}
