package com.frx01.yygh.service;

import com.frx01.yygh.model.hosp.Schedule;
import com.frx01.yygh.vo.hosp.ScheduleOrderVo;
import com.frx01.yygh.vo.hosp.ScheduleQueryVo;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Map;

/**
 * @author frx
 * @version 1.0
 * @date 2022/10/27  10:47
 */
public interface ScheduleService {

    //医院排班
    void save(Map<String, Object> paramMap);

    Page<Schedule> findPageDepartment(int page, int limit, ScheduleQueryVo scheduleQueryVo);

    void remove(String hoscode, String hosScheduleId);

    //根据 医院编号和科室编号 查询排班规则数据
    Map<String, Object> getRlueSchedule(long page, long limit, String hoscode, String depcode);

    //根据医院编号、科室编号和工作日期，查询排班详细信息
    List<Schedule> getDetailSchedule(String hoscode, String depcode, String workDate);

    //获取可预约的排版数据
    Map<String,Object> getBookingScheduleRule(Integer page, Integer limit, String hoscode, String depcode);

    //根绝排班id获取排班数据
    Schedule getScheduleById(String scheduleId);

    //根据排班id获取预约下单数据
    ScheduleOrderVo getScheduleOrderVo(String scheduleId);

    //更新排版数据
    void update(Schedule schedule);
}
