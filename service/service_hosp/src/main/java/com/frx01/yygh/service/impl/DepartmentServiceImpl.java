package com.frx01.yygh.service.impl;

import com.alibaba.fastjson.JSONObject;

import com.frx01.yygh.model.hosp.Department;
import com.frx01.yygh.repository.DepartmentRepository;
import com.frx01.yygh.service.DepartmentService;
import com.frx01.yygh.vo.hosp.DepartmentQueryVo;
import com.frx01.yygh.vo.hosp.DepartmentVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author frx
 * @version 1.0
 * @date 2022/10/27  8:45
 */
@Service
public class DepartmentServiceImpl implements DepartmentService {

    @Autowired
    private DepartmentRepository departmentRepository;

    //上传科室接口
    @Override
    public void save(Map<String, Object> paramMap) {

        //paramMap 转换成department对象
        String paramMapString = JSONObject.toJSONString(paramMap);
        Department department = JSONObject.parseObject(paramMapString, Department.class);

        //根据医院编号 和 科室编号查询
        Department departmentExist =
                departmentRepository.getDepartemntByHoscodeAndDepcode(department.getHoscode(),department.getDepcode());

        //判断
        if(departmentExist!=null){
            departmentExist.setUpdateTime(new Date());
            departmentExist.setIsDeleted(0);
            departmentRepository.save(departmentExist);
        } else {
            department.setCreateTime(new Date());
            department.setUpdateTime(new Date());
            department.setIsDeleted(0);
            departmentRepository.save(department);
        }
    }

    /**
     * 查询科室的接口
     * @param page
     * @param limit
     * @param departmentQueryVo
     * @return
     */
    @Override
    public Page<Department> findPageDepartment(int page, int limit, DepartmentQueryVo departmentQueryVo) {

        //创建PageAble对象 设置当前页和每页的记录数
        //0是第一页
        Pageable pageable = PageRequest.of(page-1,limit);
        //创建Example对象
        ExampleMatcher matcher = ExampleMatcher.matching()
                .withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING)
                .withIgnoreCase(true);
        Department department = new Department();
        BeanUtils.copyProperties(departmentQueryVo,department);
        department.setIsDeleted(0);
        Example<Department> example = Example.of(department,matcher);

        Page<Department> page1 = departmentRepository.findAll(example, pageable);
        return page1;
    }

    /**
     * 删除科室接口
     * @param hoscode
     * @param depcode
     */
    @Override
    public void remove(String hoscode, String depcode) {

        //根据医院编号和科室编号查询科室信息
        Department departemnt =
                departmentRepository.getDepartemntByHoscodeAndDepcode(hoscode, depcode);
        if(departemnt!=null){
            //调用方法删除
            departmentRepository.deleteById(departemnt.getId());
        }
    }

    //根据医院的编号，查询医院科室的列表
    @Override
    public List<DepartmentVo> findDeptTree(String hoscode) {

        //创建List集合，用于最终数据封装
        List<DepartmentVo> result = new ArrayList<>();

        //根据医院编号，查询医院所有的科室的信息
        Department departmentQuery = new Department();
        departmentQuery.setHoscode(hoscode);
        Example<Department> example = Example.of(departmentQuery);
        List<Department> departmentList = departmentRepository.findAll(example);

        //根据大科室编号 bigcode 分组，获取大科室里面下级子科室
        Map<String, List<Department>> departmentMap =
                departmentList.stream().collect(Collectors.groupingBy(Department::getBigcode));
        //遍历map集合
        for(Map.Entry<String,List<Department>> entry : departmentMap.entrySet()){
            //大科室编号
            String bigCode = entry.getKey();
            //大科室编号对应的全部数据
            List<Department> departmentList1 = entry.getValue();

            //封装大科室
            DepartmentVo departmentVo1 = new DepartmentVo();
            departmentVo1.setDepcode(bigCode);
            departmentVo1.setDepname(departmentList1.get(0).getBigname());

            //封装小科室
            List<DepartmentVo> children = new ArrayList<>();
            for (Department department : departmentList1) {
                DepartmentVo departmentVo2 = new DepartmentVo();
                departmentVo2.setDepcode(department.getDepcode());
                departmentVo2.setDepname(department.getDepname());
                //封装到list集合
                children.add(departmentVo2);
            }

            //把小科室list集合放到大科室的children里面去
            departmentVo1.setChildren(children);

            //放到最终的result里面去
            result.add(departmentVo1);
        }
        //返回结果
        return result;
    }

    @Override
    public String getDepName(String hoscode, String depcode) {
        Department departemnt =
                departmentRepository.getDepartemntByHoscodeAndDepcode(hoscode, depcode);
        if(departemnt != null){
            return departemnt.getDepname();
        }
        return null;
    }

    @Override
    public Department getDepartment(String hoscode, String depcode) {
        return departmentRepository.getDepartemntByHoscodeAndDepcode(hoscode,depcode);
    }
}
