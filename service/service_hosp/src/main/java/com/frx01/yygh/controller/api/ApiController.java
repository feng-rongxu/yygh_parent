package com.frx01.yygh.controller.api;


import com.frx01.helper.HttpRequestHelper;
import com.frx01.utils.MD5;
import com.frx01.yygh.Result;
import com.frx01.yygh.ResultCodeEnum;
import com.frx01.yygh.handler.YyghException;
import com.frx01.yygh.model.hosp.Department;
import com.frx01.yygh.model.hosp.Hospital;
import com.frx01.yygh.model.hosp.Schedule;
import com.frx01.yygh.service.DepartmentService;
import com.frx01.yygh.service.HospitalService;
import com.frx01.yygh.service.HospitalSetService;
import com.frx01.yygh.service.ScheduleService;
import com.frx01.yygh.vo.hosp.DepartmentQueryVo;
import com.frx01.yygh.vo.hosp.ScheduleQueryVo;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @author frx
 * @version 1.0
 * @date 2022/10/26  16:12
 */
@Api(tags = "医院管理api接口")
@RestController
@RequestMapping("/api/hosp")
public class ApiController {

    @Autowired
    private HospitalService hospitalService;

    @Autowired
    private HospitalSetService hospitalSetService;

    @Autowired
    private DepartmentService departmentService;

    @Autowired
    private ScheduleService scheduleService;


    //删除排班接口
    @PostMapping("/schedule/remove")
    public Result remove(HttpServletRequest request){

        //获取到传递过来科室的信息
        Map<String, String[]> requestMap = request.getParameterMap();
        Map<String, Object> paramMap = HttpRequestHelper.switchMap(requestMap);
        //获取医院编号和排班编号
        String hoscode = (String) paramMap.get("hoscode");
        String hosScheduleId = (String) paramMap.get("hosScheduleId");

        //签名的校验
        //1.获取医院系统传递过来的签名,签名是进行了MD5的加密
        String hosplSign = (String) paramMap.get("sign");

        //2.根据传递过来医院编码，查询数据库，查询签名
        //调用service方法
        String signKey = hospitalSetService.getSignKEY(hoscode);


        //3.把数据库查询签名进行MD5加密
        String signKeyMd5 = MD5.encrypt(signKey);

        //4.判断签名是否一致
        if(!hosplSign.equals(signKeyMd5)){
            throw new YyghException(ResultCodeEnum.SIGN_ERROR);
        }
        scheduleService.remove(hoscode,hosScheduleId);
        return Result.ok();

    }
    //查询排班接口
    @PostMapping("/schedule/list")
    public Result findSchedule(HttpServletRequest request){
        //获取到传递过来科室的信息
        Map<String, String[]> requestMap = request.getParameterMap();
        Map<String, Object> paramMap = HttpRequestHelper.switchMap(requestMap);

        //获取科室的编号
        String hoscode = (String) paramMap.get("hoscode");
        String depcode = (String) paramMap.get("depcode");
        //当前页 和 每页记录数
        int page = StringUtils.isEmpty(paramMap.get("page"))
                ? 1: Integer.parseInt((String) paramMap.get("page"));

        int limit = StringUtils.isEmpty(paramMap.get("limit"))
                ? 1: Integer.parseInt((String) paramMap.get("limit"));
        //TODO 签名校验
        //1.获取医院系统传递过来的签名,签名是进行了MD5的加密
        String hosplSign = (String) paramMap.get("sign");

        //2.根据传递过来医院编码，查询数据库，查询签名
        //调用service方法
        String signKey = hospitalSetService.getSignKEY(hoscode);


        //3.把数据库查询签名进行MD5加密
        String signKeyMd5 = MD5.encrypt(signKey);

        //4.判断签名是否一致
        if(!hosplSign.equals(signKeyMd5)){
            throw new YyghException(ResultCodeEnum.SIGN_ERROR);
        }

        ScheduleQueryVo scheduleQueryVo = new ScheduleQueryVo();
        scheduleQueryVo.setHoscode(hoscode);
        scheduleQueryVo.setDepcode(depcode);
        //调用service方法
        Page<Schedule> pageModel = scheduleService.findPageDepartment(page,limit,scheduleQueryVo);
        return Result.ok(pageModel);


    }
    //上传排班接口
    @PostMapping("/saveSchedule")
    public Result saveSchedule(HttpServletRequest request){
        //获取到传递过来科室的信息
        Map<String, String[]> requestMap = request.getParameterMap();
        Map<String, Object> paramMap = HttpRequestHelper.switchMap(requestMap);

        //获取医院和科室编号
        String hoscode = (String) paramMap.get("hoscode");
        String depcode = (String) paramMap.get("depcode");

        //1.获取医院系统传递过来的签名,签名是进行了MD5的加密
        String hosplSign = (String) paramMap.get("sign");

        //2.根据传递过来医院编码，查询数据库，查询签名
        //调用service方法
        String signKey = hospitalSetService.getSignKEY(hoscode);


        //3.把数据库查询签名进行MD5加密
        String signKeyMd5 = MD5.encrypt(signKey);

        //4.判断签名是否一致
        if(!hosplSign.equals(signKeyMd5)){
            throw new YyghException(ResultCodeEnum.SIGN_ERROR);
        }
        scheduleService.save(paramMap);
        return Result.ok();
    }
    //删除科室接口
    @PostMapping("/department/remove")
    public Result removeDepartment(HttpServletRequest request){
        //获取到传递过来科室的信息
        Map<String, String[]> requestMap = request.getParameterMap();
        Map<String, Object> paramMap = HttpRequestHelper.switchMap(requestMap);

        //获取医院和科室编号
        String hoscode = (String) paramMap.get("hoscode");
        String depcode = (String) paramMap.get("depcode");
        //TODO 签名校验
        //1.获取医院系统传递过来的签名,签名是进行了MD5的加密
        String hosplSign = (String) paramMap.get("sign");

        //2.根据传递过来医院编码，查询数据库，查询签名
        //调用service方法
        String signKey = hospitalSetService.getSignKEY(hoscode);


        //3.把数据库查询签名进行MD5加密
        String signKeyMd5 = MD5.encrypt(signKey);

        //4.判断签名是否一致
        if(!hosplSign.equals(signKeyMd5)){
            throw new YyghException(ResultCodeEnum.SIGN_ERROR);
        }
        departmentService.remove(hoscode,depcode);
        return Result.ok();
    }

    //查询科室的接口
    @PostMapping("/department/list")
    public Result finDepartment(HttpServletRequest request){
        //获取到传递过来科室的信息
        Map<String, String[]> requestMap = request.getParameterMap();
        Map<String, Object> paramMap = HttpRequestHelper.switchMap(requestMap);

        //获取医院的编号
        String hoscode = (String) paramMap.get("hoscode");
        //当前页 和 每页记录数
        int page = StringUtils.isEmpty(paramMap.get("page"))
                ? 1: Integer.parseInt((String) paramMap.get("page"));

        int limit = StringUtils.isEmpty(paramMap.get("limit"))
                ? 1: Integer.parseInt((String) paramMap.get("limit"));
        //TODO 签名校验
        //1.获取医院系统传递过来的签名,签名是进行了MD5的加密
        String hosplSign = (String) paramMap.get("sign");

        //2.根据传递过来医院编码，查询数据库，查询签名
        //调用service方法
        String signKey = hospitalSetService.getSignKEY(hoscode);


        //3.把数据库查询签名进行MD5加密
        String signKeyMd5 = MD5.encrypt(signKey);

        //4.判断签名是否一致
        if(!hosplSign.equals(signKeyMd5)){
            throw new YyghException(ResultCodeEnum.SIGN_ERROR);
        }
        DepartmentQueryVo departmentQueryVo = new DepartmentQueryVo();
        departmentQueryVo.setHoscode(hoscode);
        //调用service方法
        Page<Department> pageModel =
                departmentService.findPageDepartment(page,limit,departmentQueryVo);
        return Result.ok(pageModel);


    }

    //上传科室的接口
    @PostMapping("/saveDepartment")
    public Result saveDepartment(HttpServletRequest request){

        //获取到传递过来科室的信息
        Map<String, String[]> requestMap = request.getParameterMap();
        Map<String, Object> paramMap = HttpRequestHelper.switchMap(requestMap);

        //1.获取医院系统传递过来的签名,签名是进行了MD5的加密
        String hosplSign = (String) paramMap.get("sign");

        //2.根据传递过来医院编码，查询数据库，查询签名
        String hoscode = (String) paramMap.get("hoscode");
        //调用service方法
        String signKey = hospitalSetService.getSignKEY(hoscode);


        //3.把数据库查询签名进行MD5加密
        String signKeyMd5 = MD5.encrypt(signKey);

        //4.判断签名是否一致
        if(!hosplSign.equals(signKeyMd5)){
            throw new YyghException(ResultCodeEnum.SIGN_ERROR);
        }

        //调用service里面的方法
        departmentService.save(paramMap);
        return Result.ok();

    }

    //上传医院接口
    @PostMapping("/saveHospital")
    public Result saveHosp(HttpServletRequest request){
        //获取到传递过来的信息
        Map<String, String[]> requestMap = request.getParameterMap();
        Map<String, Object> paramMap = HttpRequestHelper.switchMap(requestMap);

        //1.获取医院系统传递过来的签名,签名是进行了MD5的加密
        String hosplSign = (String) paramMap.get("sign");

        //2.根据传递过来医院编码，查询数据库，查询签名
        String hoscode = (String) paramMap.get("hoscode");
        //调用service方法
        String signKey = hospitalSetService.getSignKEY(hoscode);


        //3.把数据库查询签名进行MD5加密
        String signKeyMd5 = MD5.encrypt(signKey);

        //4.判断签名是否一致
        if(!hosplSign.equals(signKeyMd5)){
            throw new YyghException(ResultCodeEnum.SIGN_ERROR);
        }

        //传输过程中“+“转换成了” “，因此我们要转换回来
        String logoData = (String) paramMap.get("logoData");
        logoData = logoData.replaceAll(" ","+");
        paramMap.put("logoData",logoData);

        //调用service方法
        hospitalService.save(paramMap);
        return Result.ok();

    }

    //查询医院
    @PostMapping("/hospital/show")
    public Result getHospital(HttpServletRequest request){
        //获取传递过来医院信息
        Map<String, String[]> requestMap = request.getParameterMap();
        Map<String, Object> paramMap = HttpRequestHelper.switchMap(requestMap);
        //获取医院编号
        String hoscode = (String) paramMap.get("hoscode");
        //1.获取医院系统传递过来的签名,签名是进行了MD5的加密
        String hosplSign = (String) paramMap.get("sign");

        //2.调用service方法
        String signKey = hospitalSetService.getSignKEY(hoscode);

        //3.把数据库查询签名进行MD5加密
        String signKeyMd5 = MD5.encrypt(signKey);

        //4.判断签名是否一致
        if(!hosplSign.equals(signKeyMd5)){
            throw new YyghException(ResultCodeEnum.SIGN_ERROR);
        }

        //调用Service方法实现  根据医院编号查询
        Hospital hospital = hospitalService.getByHosCode(hoscode);
        return Result.ok(hospital);
    }
}
