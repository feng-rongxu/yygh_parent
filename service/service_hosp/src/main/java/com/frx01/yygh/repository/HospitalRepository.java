package com.frx01.yygh.repository;

import com.frx01.yygh.model.hosp.Hospital;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author frx
 * @version 1.0
 * @date 2022/10/26  16:09
 */
@Repository
public interface HospitalRepository extends MongoRepository<Hospital,String> {

    //判断是否存在数据
    Hospital getHospitalByHoscode(String hoscode);

    //根据医院名称做查询
    List<Hospital> findHospitalByHosnameLike(String hosname);
}
