package com.frx01.yygh.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.frx01.yygh.model.hosp.HospitalSet;

/**
 * @author frx
 * @version 1.0
 * @date 2022/10/20  23:25
 */
public interface HospitalSetMapper extends BaseMapper<HospitalSet> {
}
