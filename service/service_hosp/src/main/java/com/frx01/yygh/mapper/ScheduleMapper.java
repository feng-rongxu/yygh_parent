package com.frx01.yygh.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.frx01.yygh.model.hosp.Schedule;

/**
 * @author frx
 * @version 1.0
 * @date 2022/11/12  10:50
 */
public interface ScheduleMapper extends BaseMapper<Schedule> {
}
