package com.frx01.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.frx01.yygh.model.order.PaymentInfo;

/**
 * @author frx
 * @version 1.0
 * @date 2022/11/15  21:03
 */
public interface PaymentMapper extends BaseMapper<PaymentInfo> {
}
