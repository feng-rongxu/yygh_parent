package com.frx01.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.frx01.yygh.model.order.RefundInfo;

/**
 * @author frx
 * @version 1.0
 * @date 2022/11/16  17:14
 */
public interface RefundInfoMapper extends BaseMapper<RefundInfo> {
}
