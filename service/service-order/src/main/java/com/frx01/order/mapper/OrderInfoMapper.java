package com.frx01.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.frx01.yygh.model.order.OrderInfo;

/**
 * @author frx
 * @version 1.0
 * @date 2022/11/12  9:46
 */
public interface OrderInfoMapper extends BaseMapper<OrderInfo> {
}
