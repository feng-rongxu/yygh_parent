package com.frx01.yygh.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.frx01.yygh.model.user.Patient;

import java.util.List;

/**
 * @author frx
 * @version 1.0
 * @date 2022/11/8  15:59
 */
public interface PatientService extends IService<Patient> {

    //创建就诊人的信息
    List<Patient> findAllUserId(Long userId);

    //根据id就诊人信息
    Patient getPatientId(Long id);
}
