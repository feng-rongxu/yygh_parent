package com.frx01.yygh.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.frx01.yygh.model.user.Patient;


/**
 * @author frx
 * @version 1.0
 * @date 2022/11/8  16:02
 */
public interface PatientMapper extends BaseMapper<Patient> {
}
