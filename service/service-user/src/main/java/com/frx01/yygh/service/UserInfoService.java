package com.frx01.yygh.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.frx01.yygh.model.user.UserInfo;
import com.frx01.yygh.vo.user.LoginVo;
import com.frx01.yygh.vo.user.UserAuthVo;
import com.frx01.yygh.vo.user.UserInfoQueryVo;

import java.util.Map;

/**
 * @author frx
 * @version 1.0
 * @date 2022/11/2  17:50
 */
public interface UserInfoService extends IService<UserInfo> {

    //用户手机号登录的接口
    Map<String, Object> loginUser(LoginVo loginVo);

    //根据openid判断
    UserInfo selectWxInfoOpenId(String openid);

    //用户认证
    void userAuth(Long userId, UserAuthVo userAuthVo);

    //用户列表接口（条件查询带分页）
    IPage<UserInfo> selectPage(Page<UserInfo> userInfoPage, UserInfoQueryVo userInfoQueryVo);

    /**
     * desc:用户锁定
     * @param userId
     * @param status 0：锁定 1：正常
     */
    void lock(Long userId,Integer status);

    /**
     * 根据用户的Id获取用户的详情信息
     * @param userId
     * @return
     */
    Map<String, Object> show(Long userId);

    /**
     * 认证审批
     * @param userId
     * @param authStatus
     */
    void approval(Long userId, Integer authStatus);
}
