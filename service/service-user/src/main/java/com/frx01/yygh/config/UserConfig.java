package com.frx01.yygh.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author frx
 * @version 1.0
 * @date 2022/11/2  18:22
 */
@MapperScan(basePackages = "com.frx01.yygh.mapper")
@Configuration
public class UserConfig {
}
