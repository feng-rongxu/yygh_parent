package com.frx01.yygh;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author frx
 * @version 1.0
 * @date 2022/10/25  17:12
 */
@SpringBootApplication
@EnableDiscoveryClient
@ComponentScan(basePackages = "com.frx01")
@EnableFeignClients(basePackages = "com.frx01")
public class ServiceUser {
    public static void main(String[] args) {
        SpringApplication.run(ServiceUser.class,args);
    }
}
