package com.frx01.yygh.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.frx01.yygh.model.user.UserInfo;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author frx
 * @version 1.0
 * @date 2022/11/2  17:53
 */
public interface UserInfoMapper extends BaseMapper<UserInfo> {
}
