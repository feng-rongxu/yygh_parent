package com.frx01.serviceoss.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * @author frx
 * @version 1.0
 * @date 2022/11/8  11:09
 */
public interface FileService {

    //获取上传的文件
    String uploadFile(MultipartFile file);
}
