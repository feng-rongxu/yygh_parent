package com.frx01.serviceoss.controller;

import com.frx01.serviceoss.service.FileService;
import com.frx01.yygh.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author frx
 * @version 1.0
 * @date 2022/11/8  11:07
 */
@RestController
@RequestMapping("/api/oss/file")
public class FileApiController {

    @Autowired
    private FileService fileService;

    //上传文件到阿里云oss
    @PostMapping("/fileUpload")
    public Result fileUpload(MultipartFile file){
        //获取上传的文件
        String url = fileService.uploadFile(file);
        return Result.ok(url);
    }

}
