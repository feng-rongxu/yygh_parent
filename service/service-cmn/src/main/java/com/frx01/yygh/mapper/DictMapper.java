package com.frx01.yygh.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.frx01.yygh.model.cmn.Dict;

/**
 * @author frx
 * @version 1.0
 * @date 2022/10/23  19:50
 */
public interface DictMapper extends BaseMapper<Dict> {
}
