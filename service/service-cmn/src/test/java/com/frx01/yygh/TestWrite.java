package com.frx01.yygh;

import com.alibaba.excel.EasyExcel;

import java.util.ArrayList;

/**
 * @author frx
 * @version 1.0
 * @date 2022/10/24  17:08
 */
public class TestWrite {
    public static void main(String[] args) {

        //构建数据List集合
        ArrayList<Object> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            UserData data = new UserData();
            data.setUid(i);
            data.setUsername("Lucy"+i);
            list.add(data);
        }
        String fileName = "F:\\excel\\01.xlsx";

        EasyExcel.write(fileName,UserData.class).sheet("用户信息")
                .doWrite(list);
    }
}
