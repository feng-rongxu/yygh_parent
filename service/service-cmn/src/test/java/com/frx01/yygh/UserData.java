package com.frx01.yygh;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @author frx
 * @version 1.0
 * @date 2022/10/24  17:11
 */
@Data
public class UserData {

    @ExcelProperty(value = "用户编号",index = 0)
    private int uid;

    @ExcelProperty(value = "用户名称",index = 1)
    private String username;

}
