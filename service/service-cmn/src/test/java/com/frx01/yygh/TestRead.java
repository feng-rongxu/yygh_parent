package com.frx01.yygh;

import com.alibaba.excel.EasyExcel;

/**
 * @author frx
 * @version 1.0
 * @date 2022/10/24  20:50
 */
public class TestRead {
    public static void main(String[] args) {
        //读取文件路径
        String fileName = "F:\\excel\\01.xlsx";
        //调用方法实现读取操作
        EasyExcel.read(fileName,UserData.class,new ExcelListener()).sheet().doRead();
    }
}
