package com.frx01.client;

import com.frx01.yygh.model.user.Patient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author frx
 * @version 1.0
 * @date 2022/11/12  10:26
 */
@FeignClient(value = "service-user")
@Repository
public interface PatientFeignClient {

    //根绝就诊人id获取就诊人信息
    @GetMapping("/api/user/patient/inner/get/{id}")
    public Patient getPatientOrder(@PathVariable("id") Long id);

}
