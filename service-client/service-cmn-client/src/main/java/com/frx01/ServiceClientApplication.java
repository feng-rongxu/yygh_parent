package com.frx01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author frx
 * @version 1.0
 * @date 2022/11/11  14:34
 */
@SpringBootApplication
public class ServiceClientApplication {
    public static void main(String[] args) {
        SpringApplication.run(ServiceClientApplication.class,args);
    }
}
